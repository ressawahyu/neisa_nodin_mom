<?php header('Access-Control-Allow-Origin: *'); ?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEISA | DASHBOARD</title>
    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <!-- {{-- <link rel="stylesheet" href="{{url('')}}/plugins/font-awesome/css/font-awesome.min.css"> --}} -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@1.0.1/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{url('')}}/plugins/morris/morris.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- DataTables -->
    <link rel="stylesheet" href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

     <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/brands.css">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->

    <style>
        /* ** START TAMBAH CSS IYON** */
            .dropClass {
            /* display: none; */
                visibility: hidden;
                opacity: 0;
                transition:
                all .5s ease;
                background-color: transparent;
                min-width: 160px;
                overflow: auto;
                z-index: 1;
                height: 0;
                /* margin-bottom: -5%; */
            }
            .dropClass a {
                color: black;
                padding: 12px 16px;
                /* text-decoration: none; */
                display: block;
            }
            .dropClass a:hover {background-color: rgba(255,255,255,.1);}
            .show {
                /* display: block; */
                visibility: visible;
                opacity: 1;
                height: auto;
                padding: 0.5rem 1rem;
                background-color: rgba(255,255,255,.3);
                margin-bottom: 1.5%;
                border-radius: 3%;
            }
            .putar {
                transform: rotate(90deg);
                transition: all .5s ease;
            }
            .brand-image {
                line-height: .8;
                max-height: 53px;
                width: auto;
                margin-left: 0.7rem;
                margin-right: .5rem;
                margin-top: -3px;
                float: none;
                opacity: .9;
            }
            .backgroundImg {
                width: auto;
                height: 100%;
                opacity: 1;
                position: absolute;
            }
            .backgroundImg2 {
                position: fixed;
                width: 100%;
                max-height: 56px;
                margin-left: -2%;
                opacity: 1;
            }
            .nav-item:hover {
                background-color: rgba(255,255,255,.3);
                border-radius: 5%;
                transition: all .2s ease;
            }
            .active {
                background-color: rgba(243, 255, 226, .8) !important;
                color: #343a40 !important;
                font-weight: 600;
            }
            .berisik {
                min-height:500px !important
            }
            .tesDiv {
                z-index: -1;
                opacity: .4;
                background: url(./dist/img/tesblek.png) center center
            }
            .tesDiv .bekgron{
                z-index: 1;
                opacity: 1
            }
        /* ** END TAMBAH IYON** */
        /* ** START UI BARU** */
        #bungkus {
            background: url(./dist/img/darkwall6.jpg) center center;
        }
        .garisijo {
            background-color: rgba(150, 178, 138, 1);
            height: 3px;
            width: 100%;
            position: absolute;
            bottom: 0;
            left: 0;
            /* margin-left: -6%; */
        }
        .teksboks {
            width: 85%;
            position: absolute;
            bottom: 0;
            left: 0;
        }
        .teksne{
            /* bottom: 0;
            right: 3%; */
            margin:auto;
            position: absolute;
        }
        .boksHead {
            font-size: 30px;
            /* box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19); */
            padding: 10px;
            font-weight: 500;
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            color: #96b28a;
            font-weight: bold;
            box-shadow: none;
            margin-bottom:0 !important;
        }
        .boksBody {
            height: 90px;
            /* background-color: #343a40; */
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            /* background-image: linear-gradient(to top, rgba(0,255,255,.2), rgba(150, 178, 138, .5)); */
            /* background-color: rgba(0, 0, 0, .5); */
            box-shadow: none;
        }
        .inner {
            padding:0 !important;
        }
        .card.chartcard {
            background-color:transparent;
            border: 0;
            border-radius: 0;
            box-shadow: none;
        }
        table {
            width: 100%;
            font-size: 12px;
            margin:auto;
            background-image: linear-gradient(to top, rgba(49, 113, 160,.5), rgba(1, 14, 23,.8));
        }
        /* tr:hover {
            background-color: rgba(255, 255, 255, .2)
        } */
        tr > td:hover {
            background-color: rgba(255, 255, 255, .9);
            color: #000;
        }
        thead {
            background-color: rgba(1, 14, 23,.9);
        }
        .kepanjangan {
            font-size: 10px;
        }
        .kepanjangantot {
            font-size: 12px;
        }
        /* ** END UI BARU** */

    </style>

</head>
<body class="hold-transition sidebar-mini " style="background: #f4f6f9; color: white;">
    <div class="wrapper  ">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
            <img src="{{url('')}}/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;width: 100%;">
            <!-- {{-- <img src="./dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
                width: 100%;"> --}} -->
            <!-- Left navbar links -->
            <ul class="navbar-nav" style="z-index: 999;">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
                </li>
                <li class=" navbar-brand" style="color:white; margin-left: 10%;">NEISA | DASHBOARD</li>
                <li class="nav-item">
                    <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                        color: #343a40 !important;
                        background-color: #fff;
                        position: fixed;
                        font-size: 10px;
                        right: 1%;
                        height: auto;
                        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                        text-transform: uppercase;
                        font-family: Roboto;
                        padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
                </li>
            </ul>
        </nav>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik">
            <img src="{{url('')}}/dist/img/wall3.jpg" class="backgroundImg">
            <!-- {{-- <img src="./dist/img/wall3.jpg" class="backgroundImg"> --}} -->
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="{{url('')}}/dist/img/tsel-white.png"
                style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-home"></i>
                                <p style="margin-left: 3px;">Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-newspaper"></i>
                                <p style="margin-left: 3px;">License</p>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                            <i class="nav-icon fa fa-broadcast-tower"></i>
                                <p style="margin-left: 3px;">BTS Status</p>
                            </a>
                        </li> -->

                        <li class="nav-item" style="cursor: pointer;">
                            <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                <p style="margin-left: 3px;">Create</p>
                            </a>
                        </li>
                        <div class="dropClass" id="dropId">
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Integrasi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Rehoming</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Dismantle</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Relocation</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Swap</p>
                                </a>
                            </li>
                        </div>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-project-diagram"></i>
                                <p style="margin-left: 3px;">Process Tracking</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-table"></i>
                                <p style="margin-left: 3px;">Nodin & MoM Report</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-book"></i>
                                <p style="margin-left: 3px;">Report Remedy</p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" id="bungkus" style="margin-top:55px;">
            <section class="content" >
                <ul class="nav nav-tabs">
                    <li class="nav-item"> <a class="nav-link active" href="{{url('/')}}">Daily</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('monthly')}}">Monthly</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="#">Baseline</a> </li>
                </ul>
                <div class="container-fluid" >
                    <section class="col-lg-12" id="seksion1">
                        
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title" style="text-align:center; padding-top: 30px;">Data </h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <table class="table table-striped table-bordered" id="onairBts4g">
                                            <thead>
                                                <tr>
                                                    <th>REGIONAL</th>
                                                    <th>AREA</th>
                                                    <th>VENDOR</th>
                                                    <th>ENODEB_NAME</th>
                                                    <th>CELL_NAME</th>
                                                    <th>NEID</th>
                                                    <th>SITEID</th>
                                                    <th>TAC</th>
                                                    <th>ENODEBID</th>
                                                    <th>CI</th>
                                                    <th>EARNFCN</th>
                                                    <th>PID</th>
                                                    <th>FREQUENCY</th>
                                                    <th>BANDTYPE</th>
                                                    <th>BANDWITH</th>
                                                    <th>JMLH_ENODEB</th>
                                                    <th>JMLH_CELL</th>
                                                    <th>METRO_E</th>
                                                    <th>OWNER_LINK</th>
                                                    <th>TIPE_LINK</th>
                                                    <th>FAR_END_LINK</th>
                                                    <th>TOTAL_BANDWITH</th>
                                                    <th>TANGGAL_ONAIR_LEASE_LINE</th>
                                                    <th>SITE_SIMPUL</th>
                                                    <th>JUMLAH_SITE_UNDER_SIMPUL</th>
                                                    <th>STATUS_LOKASI</th>
                                                    <th>CLUSTER_SALES</th>
                                                    <th>TYPE_BTS</th>
                                                    <th>STATUS</th>
                                                    <th>NEW_EXISTING</th>
                                                    <th>ONAIR</th>
                                                    <th>DATE_ONAIR</th>
                                                    <th>KPI_PASS</th>
                                                    <th>DATE_KPI_PASS</th>
                                                    <th>REMARK</th>
                                                    <th>DEPARTEMENT</th>
                                                    <th>TECHNICAL_AREA</th>
                                                    <th>LONGITUDE</th>
                                                    <th>LATITUDE</th>
                                                    <th>ALAMAT</th>
                                                    <th>KELURAHAN</th>
                                                    <th>KECAMATAN</th>
                                                    <th>KABUPATEN</th>
                                                    <th>PROVINSI</th>
                                                    <th>TOWER_PROVIDER</th>
                                                    <th>NAMA_TOWER_PROVIDER</th>
                                                    <th>STATUS_PLN</th>
                                                    <th>VENDOR_FMC</th>
                                                    <th>INPUT_DATE</th>
                                                    <th>STATUS_BTS</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> 

                    </section><!-- SECTION 1 -->
                </div><!-- CONTAINER-FLUID -->
            </section><!-- CONTENT -->
        </div><!-- CONTENT WRAPPER -->
    </div><!-- WRAPPER -->
</div> <!-- BODOY -->

<footer class="main-footer">
    <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
</footer>
<!-- Control Sidebar -->
<!-- <aside class="control-sidebar control-sidebar-dark">
</aside> -->
<!-- /.control-sidebar -->

<!-- KUMPULAN SCRIPT  -->
    <!--  -->
        <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
        <script src="{{url('')}}/plugins/datatables/jquery.dataTables.js"></script>
        <script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.js"></script>
        <script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
        <script src="{{url('')}}/dist/js/adminlte.min.js"></script>
        <script src="{{url('')}}/dist/js/demo.js"></script>
        <script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{url('')}}/plugins/morris/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="{{url('')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{url('')}}/plugins/knob/jquery.knob.js"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script src="{{url('')}}/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="{{url('')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="{{url('')}}/dist/js/adminlte.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{url('')}}/dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{url('')}}/dist/js/demo.js"></script>
        <script src="{{url('')}}/plugins/chartjs-old/Chart.min.js"></script>

        <script type="text/javascript">
            function logout(){
                sessionStorage.remove('token', '');
                window.location.href = "{{env('APP_URL')}}/landingPage/";
            }
        </script>
        <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script src="{{url('')}}/plugins/jQueryUI/jquery-ui.min.js"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>

        <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
         
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

        <script>

                $(document).ready(function(){
                    let dataTable =	$('#onairBts4g').DataTable({
                        processing: true, 
                        serverSide: false,
                        ajax: {
                            url: "http://localhost/apis-btsonair/public/api/onair_bts_4g?limit=1000",
                            dataSrc: '', 
                            contentType: "application/json"
                           },  
                        method: "GET",	 
                        columns: [
                            { data: 'REGIONAL', name: 'REGIONAL' },
                            { data: 'AREA', name: 'AREA' },
                            { data: 'VENDOR', name: 'VENDOR' },
                            { data: 'ENODEB_NAME', name: 'ENODEB_NAME' },
                            { data: 'CELL_NAME', name: 'CELL_NAME' },
                            { data: 'NEID', name: 'NEID' },                            				
                            { data: 'SITEID', name: 'SITEID' },
                            { data: 'TAC', name: 'TAC' },
                            { data: 'ENODEBID', name: 'ENODEBID' },
                            { data: 'CI', name: 'CI' },
                            { data: 'EARNFCN', name: 'EARNFCN' },
                            { data: 'PID', name: 'PID' },
                            { data: 'FREQUENCY', name: 'FREQUENCY' },
                            { data: 'BANDTYPE', name: 'BANDTYPE' },
                            { data: 'BANDWITH', name: 'BANDWITH' },
                            { data: 'JMLH_ENODEB', name: 'JMLH_ENODEB' },
                            { data: 'JMLH_CELL', name: 'JMLH_CELL' },
                            { data: 'METRO_E', name: 'METRO_E' },
                            { data: 'OWNER_LINK', name: 'OWNER_LINK' },
                            { data: 'TIPE_LINK', name: 'TIPE_LINK' },
                            { data: 'FAR_END_LINK', name: 'FAR_END_LINK' },
                            { data: 'TOTAL_BANDWITH', name: 'TOTAL_BANDWITH' },
                            { data: 'TANGGAL_ONAIR_LEASE_LINE', name: 'TANGGAL_ONAIR_LEASE_LINE' },
                            { data: 'SITE_SIMPUL', name: 'SITE_SIMPUL' },                            				
                            { data: 'JUMLAH_SITE_UNDER_SIMPUL', name: 'JUMLAH_SITE_UNDER_SIMPUL' },
                            { data: 'STATUS_LOKASI', name: 'STATUS_LOKASI' },
                            { data: 'CLUSTER_SALES', name: 'CLUSTER_SALES' },
                            { data: 'TYPE_BTS', name: 'TYPE_BTS' },
                            { data: 'STATUS', name: 'STATUS' },
                            { data: 'NEW_EXISTING', name: 'NEW_EXISTING' },
                            { data: 'ONAIR', name: 'ONAIR' },
                            { data: 'DATE_ONAIR', name: 'DATE_ONAIR' },
                            { data: 'KPI_PASS', name: 'KPI_PASS' },
                            { data: 'DATE_KPI_PASS', name: 'DATE_KPI_PASS'},
                            { data: 'REMARK', name: 'REMARK' },
                            { data: 'DEPARTEMENT', name: 'DEPARTEMENT' },
                            { data: 'TECHNICAL_AREA', name: 'TECHNICAL_AREA' },
                            { data: 'LONGITUDE', name: 'LONGITUDE' },
                            { data: 'LATITUDE', name: 'LATITUDE' },
                            { data: 'ALAMAT', name: 'ALAMAT' },
                            { data: 'KELURAHAN', name: 'KELURAHAN' },
                            { data: 'KECAMATAN', name: 'KECAMATAN' },                            				
                            { data: 'KABUPATEN', name: 'KABUPATEN' },
                            { data: 'PROVINSI', name: 'PROVINSI' },
                            { data: 'TOWER_PROVIDER', name: 'TOWER_PROVIDER' },
                            { data: 'NAMA_TOWER_PROVIDER', name: 'NAMA_TOWER_PROVIDER' },
                            { data: 'STATUS_PLN', name: 'STATUS_PLN' },
                            { data: 'VENDOR_FMC', name: 'VENDOR_FMC' },
                            { data: 'INPUT_DATE', name: 'INPUT_DATE' },
                            { data: 'STATUS_BTS', name: 'STATUS_BTS' },
                            ],
                            "pageLength": 10,
                            "scrollX": true,
                               
                    }); 
                }); 

        </script>

    <script>
        // *** START MONTH AND YEAR -1
            let listMonthLong = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
            let dt = new Date();
            let m = dt.getMonth()
            let n = dt.getMonth() - 1;
            let currYear  = dt.getFullYear();
            if (n < 0)
                {
                    n = 11;
                    currYear = currYear - 1;
                }
            document.getElementById("bulan").innerHTML = listMonthLong[n];
            document.getElementById("tahun").innerHTML = currYear.toString();
            document.getElementById("bulan2").innerHTML = listMonthLong[n];
            document.getElementById("tahun2").innerHTML = currYear.toString();
            document.getElementById("bulan3").innerHTML = listMonthLong[m];
        // *** END MONTH AND YEAR -1
        function dropDead() {
            document.getElementById("dropIcon").classList.toggle('putar');
            document.getElementById("dropId").classList.toggle("show");
        }
        $("#nav-open").click(function(){
            if (document.getElementById("wrapper").style.padding == "0px") {
                $("#wrapper").css('padding-left', '150px');
                $("#sidebar-wrapper").css('width', '150px');
            }else {
                document.getElementById("wrapper").style.padding = "0";
                document.getElementById("sidebar-wrapper").style.width = "0";
            }
        });
        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }
        $(document).ready(function(){
        //
            // $.ajax({
            //     url:"http://10.54.36.49/bts-rekon/api/get_summary_all",
            //     type:"get",
            //     success: function(result){
            //         var hasil = JSON.parse(result);

            //         $('#total_on_2g').html(hasil.total_on_2g.toLocaleString('en'));
            //         $('#new_2g').html(hasil.new_2g);
            //         $('#existing_2g').html(hasil.existing_2g);
            //         $('#dismantle_2g').html(hasil.dismantle_2g);
            //         $('#total_on_3g').html(hasil.total_on_3g.toLocaleString('en'));
            //         $('#new_3g').html(hasil.new_3g);
            //         $('#existing_3g').html(hasil.existing_3g);
            //         $('#dismantle_3g').html(hasil.dismantle_3g);
            //         $('#total_on_4g').html(hasil.total_on_4g.toLocaleString('en'));
            //         $('#new_4g').html(hasil.new_4g);
            //         $('#existing_4g').html(hasil.existing_4g);
            //         $('#dismantle_4g').html(hasil.dismantle_4g);
            //         $('#total_on_total').html(hasil.total_on_total.toLocaleString('en'));
            //         $('#new_total').html(hasil.new_total);
            //         $('#existing_total').html(hasil.existing_total);
            //         $('#dismantle_total').html(hasil.dismantle_total);
            //         // console.log(hasil);
            //     }
            // });
        //
        // *** START ON AIR BOX ***

            // Data On Air 4G
            $.ajax({
                url:"http://10.54.36.49/change2/public/btsonair4g",
                type:"get",
                success: function(result){

                    for(var i=0;i<result.length;i++){

                        var neid = [];
                        neid.push(result[i].neid);

                        $('#neid').html(result[i].neid);
                        $('#date_onair').html(result[i].date_onair);
                    }


                    console.log(neid);

                }
            });

            // 2G
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/dashboard_2g",
                type:"get",
                success: function(result){
                    $('#onair_2g').html(result.onair_2g);
                    $('#new_2g').html(result.new_2g);
                    $('#dismantle_2g').html(result.dismantle_2g);
                }
            });

            // 3G
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/dashboard_3g",
                type:"get",
                success: function(result){
                    $('#onair_3g').html(result.onair_3g);
                    $('#new_3g').html(result.new_3g);
                    $('#dismantle_3g').html(result.dismantle_3g);
                }
            });

            // 4G
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/dashboard_4g",
                type:"get",
                success: function(result){
                    $('#onair_4g').html(result.onair_4g);
                    $('#new_4g').html(result.new_4g);
                    $('#dismantle_4g').html(result.dismantle_4g);
                }
            });

            // TOTAL
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/dashboard_total",
                type:"get",
                success: function(result){
                    $('#onair_total').html(result.onair_total);
                    $('#new_total').html(result.new_total);
                    $('#dismantle_total').html(result.dismantle_total);
                }
            });

        // *** END ON AIR BOX ***

        // *** START TABLE BTS SUMMARY ***
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/bts_summary_2",
                type:"get",
                success: function(result){
                    $('#resume_sysinfo').empty();
                    for (var i = 0; i < result.length-1; i++) {
                        // console.log(result[i].regional);
                        $('#resume_sysinfo').append('<tr><td>'+result[i].regional+'</td><td>'+result[i].data1+'</td><td>'+result[i].data2+'</td><td>'+result[i].data3+'</td><td>'+result[i].data4+'</td><td>'+result[i].data5+'</td></tr>');
                    }
                }
            });

            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/bts_summary_2",
                type:"get",
                success: function(result){
                    $('#resume_sysinfo_total').empty();
                    for (var i = 11; i < result.length; i++) {
                        // console.log(result[i].regional);
                        $('#resume_sysinfo_total').append('<td style="text-transform: uppercase;">'+result[i].regional+'</td><td>'+result[i].data1+'</td><td>'+result[i].data2+'</td><td>'+result[i].data3+'</td><td>'+result[i].data4+'</td><td>'+result[i].data5+'</td>');
                    }
                }
            });
        // *** END TABLE BTS SUMMARY ***

        // *** START TABLE RESUME SITE ***
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/resume_site",
                type:"get",
                success: function(result){
                    // console.log("result", result);
                    $('#resume_site').empty();
                    for (var i = 0; i < result.length-1; i++) {
                        // console.log(result[i].regional);
                        $('#resume_site').append('<tr><td style="text-transform: uppercase;">'+result[i].regional+'</td><td>'+result[i].site_2g3g+'</td><td>'+result[i].site_2g3g4g+'</td><td>'+result[i].site_2g4g+'</td><td>'+result[i].site_2g+'</td><td>'+result[i].site_3g4g+'</td><td>'+result[i].site_3g+'</td><td>'+result[i].site_4g+'</td><td>'+result[i].total+'</td></tr>');
                    }
                }
            });

            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/resume_site",
                type:"get",
                success: function(result){
                    $('#resume_site_total').empty();
                    for (var i = result.length-1; i < result.length; i++) {
                        // console.log(result[i].regional);
                        $('#resume_site_total').append('<td style="text-transform: uppercase;">'+result[i].regional+'</td><td>'+result[i].site_2g3g+'</td><td>'+result[i].site_2g3g4g+'</td><td>'+result[i].site_2g4g+'</td><td>'+result[i].site_2g+'</td><td>'+result[i].site_3g4g+'</td><td>'+result[i].site_3g+'</td><td>'+result[i].site_4g+'</td><td>'+result[i].total+'</td>');
                    }
                }
            });
        // *** END TABLE RESUME SITE ***

        // *** START TABLE RESUME NE ***
            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/resume_ne",
                type:"get",
                success: function(result){
                    $('#resume_ne').empty();
                    for (var i = 0; i < result.length-1; i++) {
                        // console.log(result[i].regional);
                        $('#resume_ne').append('<tr><td colspan="2">'+result[i].regional+'</td><td colspan="2">'+result[i].vendor+'</td><td>'+result[i].bsc+'</td><td>'+result[i].site+'</td><td>'+result[i].bts+'</td><td>'+result[i].sec2g+'</td><td>'+result[i].trx+'</td><td>'+result[i].rnc+'</td><td>'+result[i].f1f2+'</td><td>'+result[i].sec6+'</td><td>'+result[i].f3+'</td><td>'+result[i].btshtl+'</td><td>'+result[i].sec3g+'</td><td>'+result[i].enodeb+'</td><td>'+result[i].sec4g+'</td></tr>');
                    }
                }
            });

            $.ajax({
                url:"http://10.54.36.49/api-btsonair/public/api/resume_ne",
                type:"get",
                success: function(result){
                    $('#resume_ne_total').empty();
                    for (var i = 16; i < result.length; i++) {
                        // console.log(result[i].regional);
                        $('#resume_ne_total').append('<td colspan="4"style="text-align:center;vertical-align:middle"> TOTAL </td><td class="kepanjangantot">'+result[i].bsc+'</td><td class="kepanjangantot">'+result[i].site+'</td><td class="kepanjangantot">'+result[i].bts+'</td><td class="kepanjangantot">'+result[i].sec2g+'</td><td class="kepanjangantot">'+result[i].trx+'</td><td class="kepanjangantot">'+result[i].rnc+'</td><td class="kepanjangantot">'+result[i].f1f2+'</td><td class="kepanjangantot">'+result[i].sec6+'</td><td class="kepanjangantot">'+result[i].f3+'</td><td class="kepanjangantot">'+result[i].btshtl+'</td><td class="kepanjangantot">'+result[i].sec3g+'</td><td class="kepanjangantot">'+result[i].enodeb+'</td><td class="kepanjangantot">'+result[i].sec4g+'</td>');
                    }
                }
            });
        // *** END TABLE RESUME NE ***

        // *** START TABLE RESUME NODIN BTS ***

        $.ajax({
            url:"http://10.54.36.49/api-btsonair/public/api/bts_summary_1",
            type:"get",
            success: function(result){

                $('#resume_nodin_bts_tanggal').append('<td colspan="2" style="text-transform: uppercase;">Radio Network Capacity Built (Unit)</td>');
                for (var i = 0; i < result.length; i++) {

                        const monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                        ];

                        var d = new Date(result[i].date),
                        month = monthNames[d.getMonth()],
                        year = d.getFullYear();

                        var b = month +" " + year;

                    $('#resume_nodin_bts_tanggal').append('<td>'+b+'</td>');
                }

                $('#resume_nodin_bts_2g_onair').append('<td colspan="2">BTS 2G - On Air This Year</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_2g_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_2g+'</td>');
                }

                $('#resume_nodin_bts_2g_dismantled').append('<td colspan="2">BTS 2G - Dismantled</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_2g_dismantled').append('<td style="text-align:center;vertical-align:middle"> '+result[i].offair_2g+'</td>');
                }

                $('#resume_nodin_bts_2g_total').append('<td colspan="2">BTS 2G - Total On Air</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_2g_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_2g+'</td>');
                }

                $('#resume_nodin_bts_3g_onair').append('<td colspan="2">BTS 3G - On this year</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_3g_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_3g+'</td>');
                }

                $('#resume_nodin_bts_3g_dismantled').append('<td colspan="2">BTS 3G - Dismantledr</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_3g_dismantled').append('<td style="text-align:center;vertical-align:middle">'+result[i].offair_3g+'</td>');
                }

                $('#resume_nodin_bts_3g_total').append('<td colspan="2">BTS 3G - Total On Air</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_3g_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_3g+'</td>');
                }

                $('#resume_nodin_bts_4g_onair').append('<td colspan="2">BTS 4G - On this year</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_4g_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_4g+'</td>');
                }

                $('#resume_nodin_bts_4g_dismantled').append('<td colspan="2">BTS 4G - Dismantled</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_4g_dismantled').append('<td style="text-align:center;vertical-align:middle">'+result[i].offair_4g+'</td>');
                }

                $('#resume_nodin_bts_4g_total').append('<td colspan="2">BTS 4G - Total On Air</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_bts_4g_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_4g+'</td>');
                }

                $('#resume_nodin_trx_onair').append('<td colspan="2">TRX - On Air this year</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_trx_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_trx+'</td>');
                }

                $('#resume_nodin_trx_dismantled').append('<td colspan="2">TRX - Dismantled</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_trx_dismantled').append('<td style="text-align:center;vertical-align:middle">'+result[i].offair_trx+'</td>');
                }

                $('#resume_nodin_trx_total').append('<td colspan="2">TRX - Total On Air</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_trx_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_trx+'</td>');
                }

                $('#resume_nodin_jumlah_bts').append('<td colspan="2">Jumlah BTS (2G, 3G & 4G)</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_jumlah_bts').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_all+'</td>');
                }

                $('#resume_nodin_jumlah_site_2g').append('<td colspan="2">Jumlah Site 2G Only</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_jumlah_site_2g').append('<td style="text-align:center;vertical-align:middle">'+result[i].only_2g+'</td>');
                }

                $('#resume_nodin_jumlah_site_3g').append('<td colspan="2">Jumlah Site 3G Only</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_jumlah_site_3g').append('<td style="text-align:center;vertical-align:middle">'+result[i].only_3g+'</td>');
                }

                $('#resume_nodin_jumlah_site_4g').append('<td colspan="2">Jumlah Site 4G Only</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_jumlah_site_4g').append('<td style="text-align:center;vertical-align:middle">'+result[i].only_4g+'</td>');
                }

                $('#resume_nodin_total_site').append('<td colspan="2">Jumlah Site 2G 3G & 4G</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_total_site').append('<td style="text-align:center;vertical-align:middle">'+result[i].collocation+'</td>');
                }

                $('#resume_nodin_total_site_keseluruhan').append('<td colspan="2">Total Jumlah Site</td>');
                for (var i = 0; i < result.length; i++) {
                    $('#resume_nodin_total_site_keseluruhan').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_all_site+'</td>');
                }

            }
        });




        // *** END TABLE RESUME NODIN BTS ***
        });
    </script>
</body>
</html>
