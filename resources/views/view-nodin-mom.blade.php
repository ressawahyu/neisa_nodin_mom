<?php header('Access-Control-Allow-Origin: *'); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEISA | DASHBOARD</title>
    <!-- Tell the browser to be responsive to screen width -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <!-- {{-- <link rel="stylesheet" href="{{url('')}}/plugins/font-awesome/css/font-awesome.min.css"> --}} -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@1.0.1/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css">

    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    
    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/brands.css">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->
    
    <style>
        /* ** START TAMBAH CSS IYON** */
        .dropClass {
            /* display: none; */
            visibility: hidden;
            opacity: 0;
            transition:
            all .5s ease;
            background-color: transparent;
            min-width: 160px;
            overflow: auto;
            z-index: 1;
            height: 0;
            /* margin-bottom: -5%; */
        }
        .dropClass a {
            color: black;
            padding: 12px 16px;
            /* text-decoration: none; */
            display: block;
        }
        .dropClass a:hover {background-color: rgba(255,255,255,.1);}
        .show {
            /* display: block; */
            visibility: visible;
            opacity: 1;
            height: auto;
            padding: 0.5rem 1rem;
            background-color: rgba(255,255,255,.3);
            margin-bottom: 1.5%;
            border-radius: 3%;
        }
        .putar {
            transform: rotate(90deg);
            transition: all .5s ease;
        }
        .brand-image {
            line-height: .8;
            max-height: 53px;
            width: auto;
            margin-left: 0.7rem;
            margin-right: .5rem;
            margin-top: -3px;
            float: none;
            opacity: .9;
        }
        .backgroundImg {
            width: auto;
            height: 100%;
            opacity: 1;
            position: absolute;
        }
        .backgroundImg2 {
            position: fixed;
            width: 100%;
            max-height: 56px;
            margin-left: -2%;
            opacity: 1;
        }
        .nav-item:hover {
            background-color: rgba(255,255,255,.3);
            border-radius: 5%;
            transition: all .2s ease;
        }
        .active {
            background-color: rgba(243, 255, 226, .8) !important;
            color: #343a40 !important;
            font-weight: 600;
        }
        .berisik {
            min-height:500px !important
        }
        .tesDiv {
            z-index: -1;
            opacity: .4;
            background: url(./dist/img/tesblek.png) center center
        }
        .tesDiv .bekgron{
            z-index: 1;
            opacity: 1
        }
        /* ** END TAMBAH IYON** */
        /* ** START UI BARU** */
        #bungkus {
            background: url(./dist/img/darkwall6.jpg) center center;
        }
        .garisijo {
            background-color: rgba(150, 178, 138, 1);
            height: 3px;
            width: 100%;
            position: absolute;
            bottom: 0;
            left: 0;
            /* margin-left: -6%; */
        }
        .teksboks {
            width: 85%;
            position: absolute;
            bottom: 0;
            left: 0;
        }
        .teksne{
            /* bottom: 0;
            right: 3%; */
            margin:auto;
            position: absolute;
        }
        .boksHead {
            font-size: 30px;
            /* box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19); */
            padding: 10px;
            font-weight: 500;
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            color: #96b28a;
            font-weight: bold;
            box-shadow: none;
            margin-bottom:0 !important;
        }
        .boksBody {
            height: 90px;
            /* background-color: #343a40; */
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            /* background-image: linear-gradient(to top, rgba(0,255,255,.2), rgba(150, 178, 138, .5)); */
            /* background-color: rgba(0, 0, 0, .5); */
            box-shadow: none;
        }
        .inner {
            padding:0 !important;
        }
        .card.chartcard {
            background-color:transparent;
            border: 0;
            border-radius: 0;
            box-shadow: none;
        }
        table {
            width: 100%;
            font-size: 14px;
            margin:auto;
            background-image: linear-gradient(to top, rgba(49, 113, 160,.5), rgba(1, 14, 23,.8));
        }
        /* tr:hover {
            background-color: rgba(255, 255, 255, .2)
        } */
        tr > td:hover {
            background-color: rgba(255, 255, 255, .9);
            color: #000;
        }
        thead {
            background-color: rgba(1, 14, 23,.9);
        }
        .kepanjangan {
            font-size: 10px;
        }
        .kepanjangantot {
            font-size: 12px;
        }
        /* ** END UI BARU** */
        
    </style>
    
</head>
<body class="hold-transition sidebar-mini " style="background: #f4f6f9; color: white;">
    <div class="wrapper  ">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
            <img src="{{url('')}}/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;width: 100%;">
            <!-- {{-- <img src="./dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
                width: 100%;"> --}} -->
                <!-- Left navbar links -->
                <ul class="navbar-nav" style="z-index: 999;">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
                    </li>
                    <li class=" navbar-brand" style="color:white; margin-left: 10%;">NEISA | DASHBOARD</li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                        color: #343a40 !important;
                        background-color: #fff;
                        position: fixed;
                        font-size: 10px;
                        right: 1%;
                        height: auto;
                        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                        text-transform: uppercase;
                        font-family: Roboto;
                        padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
                    </li>
                </ul>
            </nav>
            
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik">
                <img src="{{url('')}}/dist/img/wall3.jpg" class="backgroundImg">
                <!-- {{-- <img src="./dist/img/wall3.jpg" class="backgroundImg"> --}} -->
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                    <img src="{{url('')}}/dist/img/tsel-white.png"
                    style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
                </a>  
                
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-home"></i>
                                    <p style="margin-left: 3px;">Dashboard</p>
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a href="http://10.54.36.49/dashboard-license" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-newspaper"></i>
                                    <p style="margin-left: 3px;">License</p>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-broadcast-tower"></i>
                                    <p style="margin-left: 3px;">BTS Status</p>
                                </a>
                            </li> -->
                            
                            <li class="nav-item" style="cursor: pointer;">
                                <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                    <p style="margin-left: 3px;">Create</p>
                                </a>
                            </li>
                            <div class="dropClass" id="dropId">
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Integrasi</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Rehoming</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Dismantle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Relocation</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Swap</p>
                                    </a>
                                </li>
                            </div>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-project-diagram"></i>
                                    <p style="margin-left: 3px;">Process Tracking</p>
                                </a>
                            </li>
                            <li class="nav-item">  
                                <a href="http://10.54.36.49/tableList" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-table"></i>  
                                    <p style="margin-left: 3px;">Nodin & MoM Report</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p style="margin-left: 3px;">Report Remedy</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>
            
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" id="bungkus" style="margin-top:55px;">
                <section class="content" >
                    
                    {{--  <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="text-align:center; padding-top: 30px;">Data Nodin</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <table class="table table-striped table-bordered" id="dataNodin">
                                        <thead>
                                            <tr>
                                                <th>Nomor</th>
                                                <th>Title</th>
                                                <th>Input Date</th>
                                                <th>Submitted By</th>
                                                <th>Approval Status</th>
                                                <th>Print PDF</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  --}}
                    
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="text-align:center; padding-top: 30px;">Data Mom</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <table class="table table-striped table-bordered" id="dataMom">
                                        <thead>
                                            <tr>
                                                <th>Nomor</th>
                                                <th>Title</th>
                                                <th>Input Date</th>
                                                <th>Submitted By</th>
                                                <th>Approval Status</th>
                                                <th>Print PDF</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                    
                </section><!-- CONTENT -->
            </div><!-- CONTENT WRAPPER -->
        </div><!-- WRAPPER -->
    </div> <!-- BODOY -->
    
    <footer class="main-footer">
        <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
    </footer>
    <!-- Control Sidebar -->
    <!-- <aside class="control-sidebar control-sidebar-dark">
    </aside> -->
    <!-- /.control-sidebar -->
    
    <!-- KUMPULAN SCRIPT  -->
    <!--  -->
    <script src="{{url('')}}/plugins/jquery/jquery.min.js"></script> 
    <script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
    <script src="{{url('')}}/dist/js/adminlte.min.js"></script>
    <script src="{{url('')}}/dist/js/demo.js"></script>
    <script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{url('')}}/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="{{url('')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{url('')}}/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{url('')}}/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="{{url('')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>

    <!-- AdminLTE App -->
    <script src="{{url('')}}/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{url('')}}/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('')}}/dist/js/demo.js"></script>
    <script src="{{url('')}}/plugins/chartjs-old/Chart.min.js"></script>
    
    <script type="text/javascript">
        function logout(){
            sessionStorage.remove('token', '');
            window.location.href = "{{env('APP_URL')}}/landingPage/";
        }
    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{url('')}}/plugins/jQueryUI/jquery-ui.min.js"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script> 

    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
     
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function(){
            let dataTable =	$('#dataNodin').DataTable({
                processing: true, 
                serverSide: false,
                ajax: {
                    url: "http://10.54.36.49/api-btsonair/public/api/table_list_nodin",
                    dataSrc: '', 
                    contentType: "application/json"
                   }, 
                method: "GET",	 
                columns: [
                    { data: 'no', name: 'no' },
                    { data: 'title', name: 'title' },
                    { data: 'input_date', name: 'input_date' },
                    { data: 'submit_by', name: 'submit_by' },
                    { data: 'aprstatus', name: 'aprstatus' },
                    { 
                        "data": "input_date",
                        "render": function(data, type, row, meta){
                           if(type === 'display'){  
                            data = '<a href="generate-pdf-nodin/' + data + '" class="btn btn-xs btn-default" ><i class="fa fa-print"></i></a>';
                           }
               
                           return data;
                        }
                     }, 						
                    ],
                    "pageLength": 6,
                    'columnDefs': [
                        {
                            "targets": 0, // your case first column
                            "className": "text-center",
                            "width": "1%"
                       },
                       {
                            "targets": 1,
                            "className": "text-center",
                       },
                       {
                            "targets": 2,
                            "className": "text-center",
                       },
                       {
                            "targets": 3,
                            "className": "text-center",
                       },
                       {
                            "targets": 4,
                            "className": "text-center",
                       },
                       {
                            "targets": 5,
                            "className": "text-center",
                       }
                       ],
            }); 
        }); 


        $(document).ready(function(){
            let dataTable =	$('#dataMom').DataTable({
                processing: true,
                serverSide: false,
                ajax: {
                    url: "http://10.54.36.49/api-btsonair/public/api/table_list_mom",
                    dataSrc: '', 
                    contentType: "application/json"
                   }, 
                method: "GET",	
                columns: [
                    { data: 'no', name: 'no' },
                    { data: 'title', name: 'title' },
                    { data: 'input_date', name: 'input_date' },
                    { data: 'submit_by', name: 'submit_by' },
                    { data: 'aprstatus', name: 'aprstatus' },
                    { 
                        "data": "input_date",
                        "render": function(data, type, row, meta){
                           if(type === 'display'){  
                            data = '<a href="generate-pdf-mom/' + data + '" class="btn btn-xs btn-default" ><i class="fa fa-print"></i></a>';
                        }
               
                           return data;
                        }
                     } 
                ],
                "pageLength": 6,
                'columnDefs': [
                    {
                        "targets": 0, // your case first column
                        "className": "text-center",
                        "width": "1%"
                   },
                   {
                        "targets": 1,
                        "className": "text-center",
                   },
                   {
                        "targets": 2,
                        "className": "text-center",
                   },
                   {
                        "targets": 3,
                        "className": "text-center",
                   },
                   {
                        "targets": 4,
                        "className": "text-center",
                   },
                   {
                        "targets": 5,
                        "className": "text-center",
                   }
                   ],                
            });
        });
</script>

    
</body>
</html>
