<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head> 
    <body>
        <div class="row">
            <div class="card-body" style="height:545px; padding-top: 3px;">
                <div class="chart" style="height:545px;">
                    <canvas id="barChart7"></canvas>
                </div>
            </div>
        </div> 
    </body>

    <script type="text/javascript" src="{{url('')}}/js/app.js"></script>
    <!-- AdminLTE App -->
    <script src="{{url('')}}/custom/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('')}}/custom/js/demo.js"></script>
    <!-- jQuery -->
    <script src="{{url('')}}/custom/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <!-- <script src="custom/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- ChartJS 1.0.1 -->
    <script src="{{url('')}}/custom/chartjs-old/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('')}}/custom/fastclick/fastclick.js"></script>

<!-- *** LIAT INI AJA *** -->
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'http://10.54.36.49/api-btsonair/public/api/chart_onair',
            // url:'http://10.54.36.49/bts-rekon/api/get_onair_baru', 
            // http://10.54.36.49/bts-rekon/api/get_onair_baru
            type:'get',
            success: function(result){
                console.log("result", result)
                var ctx = document.getElementById("barChart7").getContext("2d");
                var gradient1 = ctx.createLinearGradient(0, 0, 0, 400);
                // gradient1.addColorStop(0, 'rgba(181, 92, 124, 0.1)'); 
                // gradient1.addColorStop(1, 'rgba(181, 92, 124, 1)'); 
                gradient1.addColorStop(0, 'rgba(48, 208, 220, .5)');    
                gradient1.addColorStop(1, 'rgba(48, 208, 220, 1)');
                var gradient2 = ctx.createLinearGradient(0, 0, 0, 400);
                gradient2.addColorStop(0, 'rgba(252, 112, 115, .5)');    
                gradient2.addColorStop(1, 'rgba(252, 112, 115, 1)');
                var gradient3 = ctx.createLinearGradient(0, 0, 0, 450);
                gradient3.addColorStop(0, 'rgba(151,122,208, .5 )');  
                gradient3.addColorStop(1, 'rgba(151,122,208, 1)');
                
                var regions = []
                var news = []
                var existings = []
                var totals = []
                //   console.log("result", result)
                $.each(result, function(idx, obj) {
                    regions.push(obj.regional);
                    news.push(obj.new);
                    existings.push(obj.existing);
                    totals.push(obj.total);
                    //    console.log("regions", regions)
                    //    console.log("onair", onairs)
                    //    console.log("total", total)
                    
                });

                var chartData = {
                    labels: regions,
                    datasets: [
                    {
                        //   label               : 'NEW',
                        fillColor           : gradient1,
                        data                : news,
                    },
                    {
                        //   label               : 'EXISTING',
                        fillColor           : gradient2,
                        data                : existings,
                    },
                    {
                        //   label               : 'TOTAL',
                        fillColor           : gradient3,
                        data                : totals,
                    },
                  ],
                };

                // var chartData = {
                //     labels: regions,
                //     datasets: [
                //     {
                //         //   label               : 'Planned',
                //         fillColor           : gradient1,
                //         strokeColor         : '#c2c7d0',
                //         pointColor          : '#fff',
                //         pointStrokeColor    : '#fff',
                //         pointHighlightFill  : '#fff',
                //         pointHighlightStroke: '#fff',
                //         data                : onairs,
                //     },
                //     {
                //         //   label               : 'On Air',
                //         fillColor           : gradient2,
                //         strokeColor         : '#c2c7d0',
                //         pointColor          : '#008080',
                //         pointStrokeColor    : '#008080',
                //         pointHighlightFill  : '#008080',
                //         pointHighlightStroke: '#008080',
                //         borderWidth         : 0,
                //         data                : onairs,
                //     },
                //     {
                //         //   label               : 'On Air',
                //         fillColor           : gradient3,
                //         data                : onairs,
                //     },
                //   ],
                // };

                
                
            
            var myBar = new Chart(ctx).Bar(chartData, {
                showTooltips: false,
               
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero        : true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines      : true,
                //String - Colour of the grid lines
                scaleGridLineColor      : '#0000',
                //Number - Width of the grid lines
                scaleGridLineWidth      : 1,
                scaleFontColor          :'#ffffff',
                scaleLineColor          : '#ffffff',
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines  : true,
                //Boolean - If there is a stroke on each bar
                barShowStroke           : false,
                //Number - Pixel width of the bar stroke
                barStrokeWidth          : 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing         : 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing       : 1,
                //String - A legend template
                // legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                //Boolean - whether to make the chart responsive
                responsive              : true,
                maintainAspectRatio     : false,
                onAnimationComplete: function () {

                var ctx = this.chart.ctx;
                ctx.font = this.scale.font;
                ctx.fillStyle = this.scale.textColor
                ctx.textAlign = "center";
                ctx.textBaseline = "bottom";

                this.datasets.forEach(function (dataset) {
                    dataset.bars.forEach(function (bar) {
                        ctx.fillText(bar.value, bar.x, bar.y - 5);
                    });
                })
                },
            });
        }
    });
});   
</script>
 <!-- *** SAMPE SINI YA *** -->

</html>
