<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });  
  
Route::get('/', 'NodinMomController@index');   

//getData to Menu Nodin & Mom
Route::get('/data-nodin-mom', 'NodinMomController@index')->name('view.nodin');
Route::get('/data-nodin/getJsonDataNodin','NodinMomController@getJsonDataNodin')->name('data.getJsonDataNodin');
Route::get('/data-mom/getJsonDataMom','NodinMomController@getJsonDataMom')->name('data.getJsonDataMom');
//generate pdf
Route::get('/generate-pdf-nodin/{date}','NodinMomController@generatePdfNodin')->name('generate.pdf.nodin');
Route::get('/generate-pdf-mom/{date}','NodinMomController@generatePdfMom')->name('generate.pdf.mom'); 

Auth::routes(); 

Route::get('/logout', 'Auth\LoginController@logout');