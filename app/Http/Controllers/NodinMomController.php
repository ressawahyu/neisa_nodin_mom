<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Api;
use PDF;

class NodinMomController extends Controller
{
    public function index()
    {
    	return view('view-nodin-mom'); 
    }   
     
    public function getJsonDataNodin(Request $request) 
    {  
        return DataTables::of(Api::dataNodin())
        ->make(true);  
    }

    public function getJsonDataMom(Request $request) 
    {
        return DataTables::of(Api::dataMom())
        ->make(true); 
    }
 
    public function generatePdfNodin($date)
    {
        $dataNodin = collect(Api::dataNodin()) 
        ->where('input_date','==',$date);

        $dataBtsSummary1 = Api::dataBtsSummary1($date); 
        $dataBtsSummary2 = Api::dataBtsSummary2($date);     

        $originalDate = $date;
        $formatDate1 = date("d-F-Y", strtotime($originalDate));
        $formatDate2 = date("F Y", strtotime($originalDate)); 
        $formatDate3 = date("d F Y", strtotime($originalDate));
        
        $pdf = PDF::loadView('pdfNodin', ['dataMom'=>$dataNodin, 'dataBtsSummary1'=>$dataBtsSummary1, 'dataBtsSummary2'=>$dataBtsSummary2, 'formatDate1'=>$formatDate1, 'formatDate2'=>$formatDate2,  'formatDate3'=>$formatDate3]);
        return $pdf->download('dataNodin.pdf'); 
    } 

    public function generatePdfMom($date)
    {                                
        $dataBtsSummary1 = Api::dataBtsSummary1($date); 
        $dataBtsSummary2 = Api::dataBtsSummary2($date);    
        $nomorNodin = Api::nomorNodin($date);              

        $dataMom = Api::dataMom();
        
        foreach($dataMom as $item) {
            if($item['input_date'] == $date) {
                $data = [
                    'title' => $item['title'],
                    'input_date' => $item['input_date'],
                    'submit_by' => $item['submit_by'],
                    'aprstatus'=> $item =['aprstatus']
                ]; 
            }
        }
  
        $originalDate = $date;
        $formatDate1 = date("d-F-Y", strtotime($originalDate));
        $formatDate2 = date("F Y", strtotime($originalDate.' -1 month'));
        $formatDate3 = date("d F Y", strtotime($originalDate)); 
        $formatDate4 = date("F Y", strtotime($originalDate.' -1 month'));
        $formatDate5 = date("d F Y", strtotime($data['input_date']));
        $formatDate6 = date("F Y", strtotime($originalDate.' -2 month'));

        $pdf = PDF::loadView('pdfMom', ['nomorNodin'=>$nomorNodin, 'dataMom'=>$data, 'dataBtsSummary1'=>$dataBtsSummary1, 'dataBtsSummary2'=>$dataBtsSummary2, 'formatDate1'=>$formatDate1, 'formatDate2'=>$formatDate2,  'formatDate3'=>$formatDate3, 'formatDate4'=>$formatDate4, 'formatDate5'=>$formatDate5, 'formatDate6'=>$formatDate6]);
        return $pdf->download('dataMom.pdf');  
    }
}
