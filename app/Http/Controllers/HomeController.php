<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use App\Export\BtsOnAirExport;
use App\Export\ResumeSiteExport;
use App\Export\ResumeNeExport;
use App\Export\ResumeNodinBtsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index'); 
    } 

    public function exportBtsOnAirCSV() 
    {
      return Excel::download(new BtsOnAirExport, 'BtsOnAir.csv');
    }

    public function exportResumeSiteCSV() 
    {
      return Excel::download(new ResumeSiteExport, 'ResumeSite.csv');
    }

    public function exportResumeNeCSV() 
    {
      return Excel::download(new ResumeNeExport, 'ResumeNe.csv');
    }

    public function exportResumeNodinBtsCSV() 
    {
      return Excel::download(new ResumeNodinBtsExport, 'ResumeNodinBts.csv');
    }

    public function baseline()
    {
        return view('baseline');
    }

    public function monthly()
    {
        return view('monthly');
    }
}
