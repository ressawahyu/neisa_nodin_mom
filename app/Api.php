<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Api extends Model
{     
    const URL_BTSONAIR = 'http://10.54.36.49/api-btsonair/public/';
    // const URL_BTSONAIR = 'http://localhost/apis-btsonair/public/';
             
    static function nomorNodin($date) 
    {
        $url = self::URL_BTSONAIR.'api/nomorNodin?date='.$date;
        $client = new \GuzzleHttp\Client(); 
        try { 
            $res = $client->request('GET', $url); 
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dataNodin() 
    {
        $url = self::URL_BTSONAIR.'api/table_list_nodin';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    }

    static function dataMom()
    {
        $url = self::URL_BTSONAIR.'api/table_list_mom';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        }

        return json_decode($res->getBody(), true);
    } 

    static function dataBtsSummary1($date)
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_1?date='.$date;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataBtsSummary2($date)
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_2_monthly1?date='.$date;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataBtsOnAir()
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_2';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeSite()
    {
        $url = self::URL_BTSONAIR.'api/resume_site';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeNe()
    {
        $url = self::URL_BTSONAIR.'api/resume_ne';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeNodinBts()
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_1';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi");
        } 

        return json_decode($res->getBody(), true);
    }

}
